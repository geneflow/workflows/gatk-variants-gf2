GATK Variant Calling Workflow
=============================

Version: 0.1

Trim, align, and call variants with GATK Haplotypecaller

Inputs
------

1. input_folder: Input folder containing FASTQ pairs 

2. reference_sequence: Reference sequence FASTA file

Parameters
----------

1. threads: CPU threads used for alignment. Default: 2

2. sample_ploidy: Ploidy of sample. Default: 1

3. filter_expression: Filter criteria for variants. Default: QD < 2.0 || FS > 60.0 || MQ < 40.0 || DP < 10

4. pair_hmm_threads: Number of threads for GATK native pair HMM. Default: 4
